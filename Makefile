P = arg block cli ln ln0 bb util visit
BIN = $(HOME)/bin
B = $(BIN)

p = vi

install:
	mkdir -p "$B"
	install0 () ( cp "$$f" "$B/$p.$$f"; ) ;  \
	for f in $P; do install0; done
test:; atest $P test/*

.PHONY: install test
