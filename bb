#!/usr/bin/awk -f

BEGIN {
    args()
    write()
}

function args() {
    S = 0 # symmetric box
    M = 0 # margin

    for (;;) {
	if      (ARGV[1] == "s") {S = 1; shift()}
	else if (ARGV[1] == "m") {
	    M = 1; shift()
	    m = ARGV[1]; shift()
	}
	else break
    }

    if (S) argv_sym(); else argv_nsym();
    if (M) margin()
    to_origin()
}

function to_origin() { # convert to origin and spacing
    ox = xl; oy = yl; oz = zl
    sx = xh - xl; sy = yh - yl; sz = zh - zl
}

function margin() {
    if (m ~ /%$/) margin_per(); else margin_abs()
}

function margin_per(   Lx, Ly, Lz, mx, my, mz) {
    sub(/%/, "", m); m /= 100 # to fraction
    Lx = xh - xl; Ly = yh - yl; Lz = zh - zl

    mx = m*Lx; my = m*Ly; mz = m*Lz

    xl -= mx; yl -= my; zl -= mz
    xh += mx; yh += my; zh += mz
}


function margin_abs() {
    xl -= m; yl -= m; zl -= m
    xh += m; yh += m; zh += m
}

function argv_nsym() {
    xl = ARGV[1]; shift()
    xh = ARGV[1]; shift()
    
    if (ARGC > 1) {
	yl = ARGV[1]; shift()
	yh = ARGV[1]; shift()
    } else { yl = xl; yh = xh}

    if (ARGC > 1) {
	zl = ARGV[1]; shift()
	zh = ARGV[1]; shift()
    } else { zl = yl; zh = yh }
}

function argv_sym() {
    xh = ARGV[1]; shift()
    
    if (ARGC > 1) { yh = ARGV[1]; shift() }
    else            yh = xh

    if (ARGC > 1) { zh = ARGV[1]; shift(); }
    else            zh = yh
    
    xl = -xh; yl = -yh; zl = -zh
}

function write() {
    printf "# vtk DataFile Version 2.0\n"
    printf "vtk output\n"
    printf "ASCII\n"
    printf "DATASET STRUCTURED_POINTS\n"
    printf "DIMENSIONS 2 2 2\n"
    printf "ORIGIN %s %s %s\n", ox, oy, oz
    printf "SPACING %s %s %s\n", sx, sy, sz
}

function shift(  i) { for (i = 2; i < ARGC; i++) ARGV[i-1] = ARGV[i]; ARGC-- }

# TEST: vi.bb.t0
# vi.bb m 10% s 10 20 30 > bb.out.vtk
#
